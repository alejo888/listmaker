package com.example.listmaker

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

interface ListSelectionListener {
    fun listItemSelected(list: TaskList)
}

class ListsRecyclerViewAdapter(
    private val lists: ArrayList<TaskList> = ArrayList(),
    val selectionListener: ListSelectionListener): RecyclerView.Adapter<ListViewHolder>() {

    //private val mainList = arrayOf("Shopping List", "Homework", "Chore")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder, parent, false)

        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        //return mainList.size
        return lists.size
    }

    fun addList(list: TaskList) {
        lists.add(list)
        // envia una señal al recycleView
        notifyItemInserted(lists.size -1)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.listItemId.text = (position + 1).toString()
        //holder.listItemTitle.text = mainList[position]
        holder.listItemTitle.text = lists[position].name

        holder.itemView.setOnClickListener {
            selectionListener.listItemSelected(lists[position])
        }
    }

}